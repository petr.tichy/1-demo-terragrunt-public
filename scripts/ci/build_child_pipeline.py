import jinja2
import os


def main():
    all_paths = [r.strip().split("/")
                 for r, _, _ in os.walk('./terragrunt') if len(r.split('/')) == 5]
    modules = [{"region": p[2], "env": p[3], "module": p[4]}
               for p in all_paths]

    for module in modules:
        common_file = None
        terragrunt_file = f"./terragrunt/{module['region']}/{module['env']}/{module['module']}/terragrunt.hcl"
        with open(terragrunt_file, "r") as file:
            for line in file:
                if "_common" in line:
                    common_file = line.strip().split('"')[1].split("/")[-1]
                    break
        module["common_file"] = common_file

    template_loader = jinja2.FileSystemLoader(
        searchpath="./scripts/ci/templates")
    template_env = jinja2.Environment(loader=template_loader)
    template_file_name = "gitlab-ci"
    template = template_env.get_template(f"{template_file_name}.j2")

    with open(f"terragrunt_{template_file_name}.yml", "w") as file:
        file.write(template.render(modules=modules))


if __name__ == "__main__":
    main()
