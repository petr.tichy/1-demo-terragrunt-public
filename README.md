# Demo of Terragrunt with Gitlab CI Parent/Child pipelines

Files contain placeholders in form of `<SOME_PLACEHOLDER>` which you need to update with your own if you want to use this config.

> **NOTE:** The SOPS encrypted secret cannot be decrypted as the keyvault and the used key don't exist anymore. You need to create your own and encrypt the file with `sops -e -i path/to/file.yaml`
