locals {
  # Automatically load environment-level and regional variables
  environment_vars        = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  environment_secret_vars = yamldecode(sops_decrypt_file(find_in_parent_folders("env_secret_vars.yml")))
  location_vars           = read_terragrunt_config(find_in_parent_folders("location.hcl"))

  administrator_login_password = local.environment_secret_vars.db_admin_password

  # Read common vars from common YAML file in the root dir
  common_vars = yamldecode(file("${get_parent_terragrunt_dir()}/../common_vars.yml"))

  # Extract out common variables for reuse
  environment     = local.environment_vars.locals.environment
  location        = local.location_vars.locals.location
  subscription_id = local.environment_vars.locals.subscription_id
  tenant_id       = local.common_vars.tenant_id
  project_name    = local.common_vars.project_name
  tags = merge(
    local.common_vars.tags,
    {
      Environment = local.environment,
      Region      = local.location,
      Owner       = "Stovik",
    }
  )

  # Expose the base source URL so different versions of the module can be deployed in different environments.
  base_source_url = "git::ssh://git@gitlab.com/stovik/terraform-modules/mssql-server.git"
}

# Common vars accross all envs - fill up sensible values (can be overriden in the specific resource dir)
inputs = {
  environment  = local.environment
  location     = local.location
  project_name = local.common_vars.project_name
}

generate "provider" {
  path      = "provider_config.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "azurerm" {
  # fail if ARM_SUBSCRIPTION_ID and ARM_TENANT_ID not provided
  tenant_id       = "${local.tenant_id}"
  subscription_id = "${local.subscription_id}"
  features {}
}
EOF
}
