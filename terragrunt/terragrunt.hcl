remote_state {
  backend = "azurerm"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config = {
    subscription_id      = "<AZURE_SUBSCRIPTION_ID>"
    tenant_id            = "<AZURE_AD_TENANT_ID>"
    resource_group_name  = "<RESOURCE_GROUP_NAME"
    storage_account_name = "<STORAGE_ACCOUNT_NAME>"
    use_azuread_auth     = true
    container_name       = "<CONTAINER_IN_STORAGE_ACCOUNT_NAME>"
    key                  = "${path_relative_to_include()}/terraform.tfstate"
  }
}