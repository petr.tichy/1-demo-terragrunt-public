include "root" {
  path = find_in_parent_folders()
}

include "common" {
  path   = "${dirname(find_in_parent_folders())}/_common/keyvault.hcl"
  expose = true
}

terraform {
  source = "${include.common.locals.base_source_url}?ref=f5fa34b20a7a48fb449fe77c66adb3194729d0ef"
  before_hook "tflint" {
    commands = ["apply", "plan", "validate"]
    execute  = ["tflint", "--terragrunt-external-tflint", "--minimum-failure-severity=error", "--config", ".tflint.hcl"]
  }
}

inputs = {
  key_vault_name             = "sops"
  tenant_id                  = include.common.locals.tenant_id
  soft_delete_retention_days = 7
  role_assignments = [
    {
      role_definition_name = "Key Vault Administrator"
      groups               = ["demo-admin"]
    },
    {
      role_definition_name = "Reader"
      groups               = ["demo-admin"]
    }
  ]
}
