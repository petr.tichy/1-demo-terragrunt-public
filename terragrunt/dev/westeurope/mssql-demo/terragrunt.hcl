include "root" {
  path = find_in_parent_folders()
}

include "common" {
  path   = "${dirname(find_in_parent_folders())}/_common/mssql_server.hcl"
  expose = true
}

terraform {
  source = "${include.common.locals.base_source_url}"
  before_hook "tflint" {
    commands = ["apply", "plan", "validate"]
    execute  = ["tflint", "--terragrunt-external-tflint", "--minimum-failure-severity=error", "--config", ".tflint.hcl"]
  }
}

dependency "keyvault" {
  config_path = "../keyvault-sops"

  mock_outputs_allowed_terraform_commands = ["validate", "plan"]
  mock_outputs = {
    key_vault = {
      id = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/rg-my-kv/providers/Microsoft.KeyVault/vaults/my-kv"
    }
  }
}

// Used with run-all
// dependencies {
//   paths = ["../keyvault-sops"]
// }

// "infracost breakdown --path ./" doesn't work if Terragrunt uses SOPS
// Use terragrunt plan -out something.tfplan && terragrunt show -json something.tfplan > something.json
// infracost breakdown --path something.json
inputs = {
  sql_server_name              = "demo"
  administrator_login          = "stovik"
  administrator_login_password = include.common.locals.administrator_login_password
  databases = {
    demo = { sku_name = "GP_S_Gen5_1" }
  }
  key_vault = {
    id = dependency.keyvault.outputs.key_vault_id
  }
  tags = (merge(
    include.common.locals, {"Muj" = "Tag"}
  ))
}
